# 前言
**mg_admin_frontend**项目是基于vue全家桶和elementui完成的PC端后台管理系统模板。
# 技术栈
less es6 vue2 vuex vue-router element-ui webpack echarts
# 项目运行
**注意：项目是基于nodejs运行，需要提前安装好nodejs环境。**

```
git clone 项目地址
cd mg_admin_frontend
npm install
npm run dev
```
然后访问```localhost:8080```即可进入登录页面。
